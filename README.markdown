# Requirements

Obviously you need the mighty [Z-Shell][] installed, but that is why you are reading this, is it not? ☺

The new version also depends on the [Oh My ZSH!][] collection. But you do not need to worry about that, as it is included in the installation process.

[Z-Shell]: http://www.zsh.org/
[Oh My ZSH!]: http://ohmyz.sh/


# Installation

1. Download this repository by pulling it like this (I prefer the `.zsh` directory, but you can use whatever you want):

		cd ~
		git clone git@gitorious.org:hook-dot-files/zsh.git .zsh

2.	Download the Oh My ZSH! collection, which resides in submodule of this repository (substitute `.zsh` with your directory):

		cd ~/.zsh/oh-my-zsh
		git submodule init
		git submodule update

3. Make the new `zshrc` file the default (if needed before that migrate the content of the existing one into `.zsh/custom`):

		ln -s ~/.zsh/zshrc ~/.zshrc

4. Log into a fresh Z-Shell and be amazed! ☺


# Usage

My install works just like any other [Oh My ZSH!][] repository with just a few differences:

* the custom directory resides in `.zsh/custom` (instead of `.zsh/oh-my-zsh/custom`, so I can track it individually;
* `zlogin` and `zlogout` scripts that run when you log into or out of your Z-Shell;
* a custom theme called `hook`;
* (soon) a few custom plugins (e.g. for [ConTeXt][]).

Other than that, just refer to [Oh My ZSH!’s documentation][docs].

I will try to upstream all of my customisations, so this list will hopefully get smaller instead of bigger in the future.

[docs]: https://github.com/robbyrussell/oh-my-zsh/wiki
[ConTeXt]: http://wiki.contextgarden.net
