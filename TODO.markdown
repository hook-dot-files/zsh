 * fix/add SVN/Git/… branch to be shown in prompt
 * add an auto-completion plugin for ConTeXt (by modifying the texexec autocompletion?)
 * alternative: test `compleat`
 * fix `<home>` and `<end>` keybindings (in Konsole?)
 * push `hook.zsh-theme` upstream
 * see if you need `gpg-agent` and/or `ssh-agent`
