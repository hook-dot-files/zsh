### Coloured Git state indicator → taken from: http://briancarper.net/blog/570/git-info-in-your-zsh-prompt
autoload -Uz vcs_info
 
zstyle ':vcs_info:*' stagedstr '%F{green}●%f'
zstyle ':vcs_info:*' unstagedstr '%F{yellow}●%f'
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{red}:%f%F{yellow}%r%f'
zstyle ':vcs_info:*' enable git svn hg
precmd () {
	if [[ -z $(git ls-files --other --exclude-standard 2> /dev/null) ]] {
		zstyle ':vcs_info:*' formats '%F{cyan}[%b%c%u%f%F{cyan}]%f'
	} else {
		zstyle ':vcs_info:*' formats '%F{cyan}[%b%c%u%f%F{red}●%f%F{cyan}]%f'
	}

	vcs_info
}

### Shows prompt character according to the VCS you are currently in or super/normal user
function prompt_char {
	git branch >/dev/null 2>/dev/null && echo '±' && return
	hg root >/dev/null 2>/dev/null && echo '☿' && return
	svn info >/dev/null 2>/dev/null && echo '⚡' && return
	echo '%#'
}

### Enables a nice prompt
setopt prompt_subst				# Enables various extentions for the prompt
autoload -U colors && colors	# Enables colours

### The left side of a normal prompt
### shows: username, hostname, (p|t)ts, working directory, standard prompt character or VCS sign; special styling for root.
PROMPT='%(!.%B%U%F{blue}%n%f%u%b.%F{blue}%n%f) at %F{magenta}%m%f on %F{yellow}%y%f in %F{cyan}%~%f
{${vcs_info_msg_0_} %(!.%F{red}$(prompt_char)%f.$(prompt_char)) }: %{$reset_color%}'

### The right side of a normal prompt
### shows: indicator when in "normal" mode in Vi mode, date and time (localised to Slovenian, but easy to change)
RPROMPT='$(vi_mode_prompt_info) %F{cyan}%D{%e.%b.%y %H.%M}%f%{$reset_color%}'
#                                          ^^^^^^^^^^^^^^ ← this is the part that defines date and time.

### Loop prompt
PROMPT2='{%_}  '

### Selection prompt
PROMPT3='{ … }  '

### So far I don't have any use for 'setopt xtrace', so I don't use this prompt
#PROMPT4=''
