# Let’s first test without it, to see if it’s still needed in oh-my-zsh

### Adds more information and/or structure to auto-completion (according to Gentoo Docs)
# zstyle ':completion:*:descriptions' format '%U%B%d%b%u' 
# zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'
