# Colours and case-insesitive search in `less`
export LESS="-Ri"

# Colours and more human sorting in `ls`
alias ls="ls -v --color=auto"
