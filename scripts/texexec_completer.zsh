_texexec() {
	local expl 
	_description files expl 'TeX File'
	_files "$expl[@]" -g '*.tex'

	_arguments -s : \
	'--check[check versions]' \
	'--figures[generate overview of figures]' \
	'--listing[list of file content]' \
	'--make[make formats]' \
	'--modules[generate module documentation]' \
	'(--mpgraphic --mpstatic)'{--mpgraphic,--mpstatic}'[process mp file to stand-alone graphics]' \
	'--mptex[process mp file]' \
	'--mpxtex[process mpx file]' \
	'(--pdfarrange --pdfcombine --pdfcopy --pdfselect --pdfsplit --pdftrim)'{--pdfarrange,--pdfcombine,--pdfcopy,--pdfselect,--pdfsplit,--pdftrim}'[post process a pdf file]' \
	'--process[process file]' \
	'--help[produce a summary of all switches and arguments]' \
	'(--alpha --beta)'{--alpha,--beta}'[run test release of ConTeXt]' \
	'(--arrange --noarrange)'{--arrange,--noarrange}'[process and arrange]' \
	'(--automprun,--nomprun)'{--automprun,--nomprun}'[intermediate MP run]' \
	'(--batchmode --nonstopmode --nompmode --silendmode)'{--batchmode,--nonstopmode,--nompmode,--silentmode}'[Modes]' \
	'(--keep,--purge,--purgeall)'{--purge,--purgeall,--keep}'[temporary file handling]' \
	'(--pdftex --luatex --xetex)'{--pdftex,--luatex,--xetex}'[TeX engine]' \
	'--color[enable color (when not yet enabled)]' \
	'(--once --simplerun)'{--once,--simplerun} \
	'--verbose[Give detailed information]' \
	'--final[add a final run without skipping]' \
	'(--centerpage --markings --mkii --paranoid --pretty --screensaver --texutil --utfbom --xpdf --combine --draft --fast --fastdisabled --forcempy--forcetexutil--forcexml --fullscreen --local --alone--nobackend--nobanner--noctx--noduplex--nomapfiles--notparanoid)'{--centerpage,--markings,--mkii,--paranoid,--pretty,--screensaver,--texutil,--utfbom,--xpdf,--combine,--draft,--fast,--fastdisabled,--forcempy,--forcetexutil,--forcexml,--fullscreen,--local,--alone,--nobackend,--nobanner,--noctx,--noduplex,--nomapfiles,--notparanoid}
}

compdef _texexec texexec